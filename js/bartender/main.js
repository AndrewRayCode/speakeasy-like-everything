/**
 * The main module
 *
 * @context atl.general
 */

(function($) {
    $(function() {
        $(document.body).append($('<div>AJS.$.bartender.setKeyForUrl(\'hello\', \'goodbye\'); AJS.$.bartender.getKeyForUrl(\'hello\');</div><div><a href="http://aray.sf.atlassian.com:2990/jira/browse/BOB-1">bob-1</a> <a href="http://aray.sf.atlassian.com:2990/jira/plugins/servlet/speakeasy/user">speakeasy</a></div>'));
        var $details,
            allLikes = [],
            user = $('#header-details-user-fullname').attr('data-username');

        if(($details = $('#issue_actions_container div.issue-data-block')).length) {

            $details.append($('<div><span class="think">Thinking...</span></div>').attr('class', 'likeable'));
            $details.append($('<div></div>').attr('class', 'users-who-liked-this-shiz'));

            $.bartender.getKeysForUrl(function(err, response) {
                if(err) {
                    if(err.status != 404) {
                        return;
                    }
                }

                if(!err) {
                    allLikes = response;
                }
                populateLikes()
            });
        }

        function populateLikes() {
            var total,
                myLikes,
                theirLikes;
                
            for(var t = 0; t < allLikes.length; t++) {
                if(allLikes[t].user == user) {

                    myLikes = allLikes[t].liked.split(',');

                    for(var q = 0; q < myLikes.length; q++) {
                        if(myLikes[q] !== '[]') {
                            setLiked($('#' + myLikes[q]).find('.likeable'));
                        }
                    }
                } else {
                    theirLikes = allLikes[t].liked.split(',');
                    for(var q = 0; q < theirLikes.length; q++) {
                        if(theirLikes[q] !== '[]') {
                            addUserTo($('#' + theirLikes[q]).find('.users-who-liked-this-shiz'), allLikes[t].user);
                        }
                    }
                }
            }

            $('#issue_actions_container div.issue-data-block div.likeable').each(function(index, item) {
                if($(item).find('.think').length) {
                    setUnliked($(item));
                }
            });
        }

        function updateLikes(callback) {
            var likes = [];
            $('#issue_actions_container div.issue-data-block div.likeable').each(function(index, item) {
                $item = $(item);

                if($item.find('.likeme[disabled]').length ||
                        $item.find('.hateme:not([disabled])').length) {
                    likes.push($item.closest('.issue-data-block').attr('id'));
                }
            });
            $.bartender.setKeyForUrl('liked', likes.toString() || '[]', callback);
        }

        function setUnliked($liker) {
            $liker.empty().append($('<button class="likeme">Like?</button>').click(function(evt) {
                $target = $(evt.target).attr('disabled', 'disabled');

                updateLikes(function() {
                    setLiked($liker);
                })
            }));
        }

        function setLiked($liker) {
            $liker.empty().text('You like this! ').append($('<button class="hateme">What? No I don\'t...</button>').click(function(evt) { 
                $target = $(evt.target).attr('disabled', 'disabled');

                updateLikes(function() {
                    setUnliked($liker);
                })
            }));
        }

        function addUserTo($container, muser) {
            $container.text($container.text() + muser + ' ');
        }

        $(document).unbind('bartender');
    });
})(AJS.$ || require('speakeasy/jquery').jQuery);


(function($) {
    //$(document).ready(function() {
    //http://localhost:2990/jira/rest/pde/1.0/asdfasdf?_=1319750503287
    var baseUrl = contextPath || alert('Context path not implemented, find it in this product, everything is about to blow up!');

    $.bartender = {
        setKeyForUrl: function(key, value, callback) {
            $.ajax(getAjaxObject('byUrl/key/' + key+ '/' + value, callback));
        },
        getKeyForUrl: function(key, callback) {
            $.ajax(getAjaxObject('byUrl/key/' + key, callback));
        },
        getKeyForUser: function(key, callback) {
            $.ajax(getAjaxObject('byUser/key/' + key, callback));
        },
        setKeyForUser: function(key, value, callback) {
            $.ajax(getAjaxObject('byUser/key/' + key, callback));
        },
        getKeysForUrl: function(callback) {
            $.ajax(getAjaxObject('byUrl/keys', callback));
        }
    };

    function getAjaxObject(url, callback) {
        return {
            url: baseUrl + '/rest/bartender/1.0/' + url,
            success: function(response) {
                callback && callback(null, response);
            },
            error: function(response) {
                callback && callback(response);
            }
        };
    }
    $(document).trigger('bartender.ready');
//});
})(AJS.$ || require('speakeasy/jquery').jQuery);
